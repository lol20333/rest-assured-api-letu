package apiLetu;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class addToCart {

    private String catalogRefIds;
    private String productId;
    private Integer quantity;
    private String pushSite;
    private String _dynSessConf;

    public addToCart(String catalogRefIds, String productId, Integer quantity, String pushSite, String _dynSessConf) {
        this.catalogRefIds = catalogRefIds;
        this.productId = productId;
        this.quantity = quantity;
        this.pushSite = pushSite;
        this._dynSessConf = _dynSessConf;
    }
}
