package apiLetu;

public class resultData {
    private String key;
    private String message;
    private String buttonText;
    private String notificationLevel;
    private boolean closable;

    public resultData() {
    }

    public resultData(String key, String message, String buttonText, String notificationLevel, boolean closable) {
        this.key = key;
        this.message = message;
        this.buttonText = buttonText;
        this.notificationLevel = notificationLevel;
        this.closable = closable;
    }

    public String getKey() {
        return key;
    }

    public String getMessage() {
        return message;
    }

    public String getButtonText() {
        return buttonText;
    }

    public String getNotificationLevel() {
        return notificationLevel;
    }

    public boolean isClosable() {
        return closable;
    }
}
