package apiLetu;

public class SessionData  {
    private String siteId;
    private boolean versionSupported;
    private long sessionConfirmationNumber;

    public SessionData() {

    }

    public SessionData(String siteId, Boolean versionSupported, Long sessionConfirmationNumber) {
        this.siteId = siteId;
        this.versionSupported = versionSupported;
        this.sessionConfirmationNumber = sessionConfirmationNumber;
    }

    public String getSiteId() {
        return siteId;
    }

    public Boolean isVersionSupported() {
        return versionSupported;
    }

    public Long getSessionConfirmationNumber() {
        return sessionConfirmationNumber;
    }
}
