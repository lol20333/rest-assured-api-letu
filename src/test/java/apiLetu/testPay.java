package apiLetu;

import io.restassured.http.ContentType;

import io.restassured.response.Response;
import net.serenitybdd.junit.runners.SerenityRunner;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static io.restassured.RestAssured.given;

public class testPay {
    private final static String URL = "https://www.preprod.letu.ru/";
    private Map<String, String> cookies;
    private String session;
    private final static String result = "Применить промокод и списать баллы можно при оформлении заказа";
    @BeforeEach
    public void initRequest(){ //Получение куки и сессии
        Response response = given()
                .get(URL + "rest/model/atg/rest/SessionConfirmationActor/getSessionConfirmationNumber?pushSite=storeMobileRU").then().extract().response();
        cookies = response.cookies();
        session = response.body().as(SessionData.class).getSessionConfirmationNumber().toString();
    }

    @Test
    public void addItemToCart(){ //Добавление товара в корзину и получение корзины

        addToCart sku1 = new addToCart("97000013", "82500010", 1, "storeMobileRU", session);
        Boolean checkResult = false;
        Response succesAddToCart = given()
                .body(sku1)
                .cookies(cookies)
                .when()
                .contentType(ContentType.JSON)
                .post(URL+"rest/model/atg/commerce/order/purchase/CartModifierActor/addItemToOrder")
                .then()
                .extract().response();

        List<resultData> response = (List<resultData>) given()
                .cookies(cookies)
                .when()
                .contentType(ContentType.JSON)
                .get(URL + "s/api/cart/v3/cart?pushSite=storeMobileRU")
                .then()
                .statusCode(200)
                .extract().body().jsonPath().getList("notificationMessages", resultData.class);
        List<String> notfications = response.stream().map(resultData::getMessage).collect(Collectors.toList());
        for (int i = 0; i < notfications.size(); i++)
        {
            checkResult |= notfications.get(i).contains(result);
           System.out.println(checkResult);
        }
        Assertions.assertTrue(checkResult);


    }



}
